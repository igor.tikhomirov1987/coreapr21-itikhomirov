package com.epam.learn.l7;

import java.util.Objects;

public class Snezhok {
    private String name;

    public Snezhok(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Snezhok{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Snezhok snezhok = (Snezhok) o;
        return Objects.equals(name, snezhok.name);
    }

    @Override
    public int hashCode() {
        return 1;
    }

    public void setName(String name) {
        this.name = name;
    }
}

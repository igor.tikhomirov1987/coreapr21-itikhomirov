package com.epam.learn.l3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class Main {
    private static boolean isTrue = true;

    public static void main(String[] args) {
        String line = null;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            line = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int finalResult = Integer.parseInt(Objects.requireNonNull(line));

        if (finalResult == 1) {
            System.out.println("green");
        } else if (finalResult == 2) {
            System.out.println("yellow");
        } else if (finalResult == 3) {
            System.out.println("red");
        } else {
            throw new RuntimeException();
        }
    }
}

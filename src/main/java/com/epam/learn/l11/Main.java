package com.epam.learn.l11;

public class Main {
    public static void main(String[] args) {
        FuncEx funcEx = new FuncEx() {
            @Override
            public String getInfo() {
                return "I am a Cat";
            }
        };

        FuncEx funcEx2 = () -> "I am a Cat";

        System.out.println(funcEx.getInfo());
        System.out.println(funcEx2.getInfo());

        FuncEx2 funcEx3 = new FuncEx2() {
            @Override
            public void getInfo(String name) {
                String result = "jack " + name;
                System.out.println("I am " + result);
            }
        };
        FuncEx2 funcEx4 = name -> System.out.println("I am " + name);
        FuncEx2 funcEx5 = name -> {
            String result = "jack " + name;
            System.out.println("I am " + result);
        };
    }
}

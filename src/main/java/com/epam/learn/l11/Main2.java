package com.epam.learn.l11;

import java.util.ArrayList;
import java.util.List;

public class Main2 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Barsik");
        list.add("Barsik");
        list.add("Murzik");
        list.add("Barsik");
        list.add("Barsik");
        list.add("Murzik");
        list.add("Barsik");
        list.add("Murzik");
        list.add("Barsik");
        list.add("Barsik");

        System.out.println(list);

        list.removeIf(O -> O.equals("Barsik"));

        System.out.println(list);
    }
}

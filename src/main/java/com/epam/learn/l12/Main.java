package com.epam.learn.l12;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    public static void main(String[] args) {
//        PrintWriter writer = null;
//
//        try {
//            // open stream
//            FileWriter out = new FileWriter("C:text.txt");
//            // придание потоковому объекту требуемых свойств
//            BufferedWriter bufferedWriter = new BufferedWriter(out);
//
//            writer = new PrintWriter(bufferedWriter);
//            writer.println("I am a sentence in a text file");
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (writer != null) {
//                writer.close();
//            }
//        }

        try (PrintWriter writer1 = new PrintWriter(new BufferedWriter(new FileWriter("text.txt")))){
            writer1.println("I am a sentence in a text file from example two");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

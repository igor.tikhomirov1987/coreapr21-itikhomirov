package com.epam.learn.l12;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class Main4 {
    public static void main(String[] args) throws IOException {
        String tmp = "diugcouegcluybearcfybbcvgoisdjfviu[],.";
        int length = tmp.length();
        char[] chars = tmp.toCharArray();

        CharArrayReader input1 = new CharArrayReader(chars);
        CharArrayReader input2 = new CharArrayReader(chars, 0, 5);

        int i;
        System.out.println("Input1 is:");
        while ((i = input1.read()) != -1) {
            System.out.print((char) i);
        }
        System.out.println("");

        System.out.println("Input2 is:");
        while ((i = input2.read()) != -1) {
            System.out.print((char) i);
        }

        System.out.println();
        Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("otput_main4"), "UTF8"));
        out.write("live");
        out.close();
    }
}

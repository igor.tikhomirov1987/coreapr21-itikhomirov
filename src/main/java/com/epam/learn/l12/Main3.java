package com.epam.learn.l12;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main3 {
    public static void main(String[] args) throws IOException {
        byte[] bytesToWord = {1, 2, 3};
        byte[] bytesRead = new byte[10];

        String FileName = "c:\\temp\\test.txt";
        FileOutputStream outputStream = null;
        FileInputStream inputStream = null;

        outputStream = new FileOutputStream(FileName);
        System.out.println("File is opened for writing...");
        outputStream.write(bytesToWord);
        System.out.println("Wrote " + bytesToWord.length + " byte.");
        outputStream.close();
        System.out.println("Finished out.");

        inputStream = new FileInputStream(FileName);
        System.out.println("File is opened for reading...");
        int bytesAvailable = inputStream.available();
        System.out.println("Ready for reading " + bytesAvailable + " bytes.");
        int count = inputStream.read(bytesRead, 0, bytesAvailable);
        System.out.println("Read: " + count + " bytes.");
        inputStream.close();
        System.out.println("Finished in.");

    }
}

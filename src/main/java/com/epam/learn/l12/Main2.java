package com.epam.learn.l12;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

public class Main2 {
    public static void main(String[] args) {
        byte[] bytes = {1, -1, 0};
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        System.out.println(inputStream.read());
        System.out.println(inputStream.read());
        System.out.println(inputStream.read());

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(10);
        outputStream.write(15);
        byte[] bytes1 = outputStream.toByteArray();
        System.out.println(Arrays.toString(bytes1));
    }
}

package com.epam.learn.l8;

import com.epam.learn.l8.impl.PizzaMSK;
import com.epam.learn.l8.impl.PizzaSpb;

public class PizzaRunner {
    private PizzaInterface pizzaMsk;
    private PizzaInterface pizzaSpb;

    public static void main(String[] args) {
        PizzaRunner runner = new PizzaRunner();
        runner.getPizzaInfo();
    }

    private void getPizzaInfo() {
        pizzaMsk = new PizzaMSK();
        pizzaSpb = new PizzaSpb();
//        pizzaSpb.sayHello();
        pizzaMsk.wash();
        pizzaSpb.wash();
        pizzaMsk.cook();
        pizzaSpb.cook();
        pizzaMsk.delivery();
        pizzaSpb.delivery();
    }
}

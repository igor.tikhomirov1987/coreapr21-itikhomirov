package com.epam.learn.l5;

public class Main3 {
    public static void main(String[] args) {
        String ex = "Barsik";
        String ex3 = "Barsik";
        String ex2 = "Bar" + "sik";
        String ex4 = new String("Barsik");

        System.out.println(ex.concat(ex2));

        System.out.println(ex == "Barsik");
        System.out.println(ex == ex3);
        System.out.println(ex == ex2);
        System.out.println(ex == ex4);
    }
}

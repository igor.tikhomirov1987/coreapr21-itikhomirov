package com.epam.learn.l5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main4 {
    public static void main(String[] args) {
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
           if (reader.readLine().equals("1")){
                new Main4().fromliteral();
           } else {
                new Main4().fromliteral2();
           }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fromliteral() {
        String name = "barsik";
        System.out.println(name == "barsik");
    }

    private void fromliteral2() {
        String name = "murzik";
        System.out.println(name == "murzik");
    }
}

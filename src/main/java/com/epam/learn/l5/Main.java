package com.epam.learn.l5;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String name = "barsik";
        String name2 = "bar";
        name2 = name2 + "sik";

        System.out.println(name == name2);
        System.out.println(name);
        System.out.println(name2);

        System.out.println(name.equals(name2));

        System.out.println(name.toLowerCase());
        System.out.println(name.toUpperCase());
        System.out.println(name.indexOf("r"));

        String value = "Abrakadabra";

        System.out.println(value.indexOf("a",6));
        System.out.println(value.indexOf(97));
        System.out.println(value.lastIndexOf(97));
        System.out.println(value.lastIndexOf("abr"));

        System.out.println(value.replace("a",""));
        String value2 = "Abrakadabra";
        System.out.println(value2.replaceAll("a",""));

        System.out.println(value.contains("akaa"));
        System.out.println(value.startsWith("Ab"));
        System.out.println(value.endsWith("ra"));

        System.out.println("   a  ss  ".trim());
        System.out.println("barsik".substring(2));
        System.out.println("barsik".substring(2, 5));

        System.out.println(Arrays.toString("barsik;murzik;pushok".split(";")));
    }
}

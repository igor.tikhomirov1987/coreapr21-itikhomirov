package com.epam.learn.l10;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class Main2 {
    public static void main(String[] args) {
        Vector vector = new Vector(4,2);
        System.out.println("Initial size: " + vector.size());
        System.out.println("Initial capacity: " + vector.capacity());
        vector.addElement(new Integer(2));
        vector.addElement(new Integer(3));
        vector.addElement(new Integer(4));
        vector.addElement(new Integer(5));
        System.out.println(vector);
        System.out.println("Total size: " + vector.size());
        System.out.println("Total capacity: " + vector.capacity());
        vector.addElement(new Double(1.2));
        System.out.println(vector);
        System.out.println("Total size: " + vector.size());
        System.out.println("Total capacity: " + vector.capacity());

        Enumeration enumeration = vector.elements();
        System.out.print("Elements in enumeration: ");
        while (enumeration.hasMoreElements()) {
            System.out.print(enumeration.nextElement() + " ");
        }

        Hashtable<String, String> hashtable = new Hashtable<>();
        for(Enumeration<String> e = hashtable.keys(); e.hasMoreElements();) {
            String s = e.nextElement();
            System.out.println(s);
        }
    }
}
